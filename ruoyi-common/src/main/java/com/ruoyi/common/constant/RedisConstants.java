package com.ruoyi.common.constant;

/**
 * @author toumakazusa
 * @since 1.0.0
 */
public class RedisConstants {
    public static final String POST_ID_KEY = "POSTID_";
    public static final String PREFIX = "UV_";
}

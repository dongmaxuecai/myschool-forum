package com.ruoyi.common.constant;

/**
 * 消息队列常量
 *
 * @author toumakazusa
 * @since 1.0.0
 */
public class RabbitMqConstants {
    public static final String POST_EXCHANGE = "POST_EXCHANGE";
    public static final String POST_PAGEVIEW_ROUTING = "POST_PAGEVIEW_ROUTING";
    public static final String POST_PAGEVIEW_QUEUE = "POST_PAGEVIEW_QUEUE";
}

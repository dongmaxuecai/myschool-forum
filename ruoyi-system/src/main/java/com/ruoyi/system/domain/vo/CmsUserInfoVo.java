package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;

/**
 * 前台用户个人信息
 *
 * @author toumakazusa
 * @since 1.0.0
 */
public class CmsUserInfoVo implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer totalViews ; //总阅读数
    private Integer postNum ; //帖子总数


    private Long userId;

    /** 用户昵称 */
    private String nickName;

    /** 用户性别 */
    private String sex;

    /** 最近登录IP属地 */
    private String region;

    /** 用户头像 */
    private String avatar;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    public Integer getTotalViews() {
        return totalViews;
    }

    public void setTotalViews(Integer totalViews) {
        this.totalViews = totalViews;
    }

    public Integer getPostNum() {
        return postNum;
    }

    public void setPostNum(Integer postNum) {
        this.postNum = postNum;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("totalViews", totalViews)
                .append("postNum", postNum)
                .append("userId", userId)
                .append("nickName", nickName)
                .append("sex", sex)
                .append("region", region)
                .append("avatar", avatar)
                .append("createTime", createTime)
                .toString();
    }
}

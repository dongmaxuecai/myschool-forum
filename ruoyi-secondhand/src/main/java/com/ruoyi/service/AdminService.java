package com.ruoyi.service;

import com.ruoyi.model.AdminModel;
import com.ruoyi.vo.PageVo;

public interface AdminService {

    AdminModel login(String accountNumber, String adminPassword);

    PageVo<AdminModel> getAdminList(int page, int nums);

    boolean addAdmin(AdminModel adminModel);


}

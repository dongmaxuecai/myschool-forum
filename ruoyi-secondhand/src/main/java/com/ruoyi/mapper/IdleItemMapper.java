package com.ruoyi.mapper;

import com.ruoyi.model.IdleItemModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface IdleItemMapper {
    int deleteByPrimaryKey(Long id);

    int insert(IdleItemModel record);

    int insertSelective(IdleItemModel record);

    IdleItemModel selectByPrimaryKey(Long id);

    List<IdleItemModel> getAllIdleItem(Long userId);

    int countIdleItem(String findValue);

    int countIdleItemByLable(int idleLabel);

    int countIdleItemByStatus(int status);

    List<IdleItemModel> findIdleItem(@Param("findValue") String findValue,@Param("begin") int begin,@Param("nums") int nums);

    List<IdleItemModel> findIdleItemByLable(@Param("idleLabel") int idleLabel,@Param("begin") int begin,@Param("nums") int nums);

    List<IdleItemModel> getIdleItemByStatus(@Param("status") int status, @Param("begin") int begin,@Param("nums") int nums);

    int updateByPrimaryKeySelective(IdleItemModel record);

    int updateByPrimaryKey(IdleItemModel record);

    List<IdleItemModel> findIdleByList(List<Long> idList);
}
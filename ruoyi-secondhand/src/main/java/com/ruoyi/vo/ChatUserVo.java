package com.ruoyi.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 聊天用户基本信息
 *
 * @author toumakazusa
 * @since 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatUserVo {
    //id
    private Long id;
    //用户名
    private String name;
    //用户头像
    private String headImg;
    //备注
    private String detail;
    //
    String img;
    //
    String lastMsg;

}

package com.ruoyi.cms.charts.service;

import com.ruoyi.cms.comment.domain.CmsComment;
import com.ruoyi.cms.message.domain.CmsMessage;
import com.ruoyi.cms.post.domain.CmsPost;

import java.util.Collection;
import java.util.List;

/**
 * @author toumakazusa
 * @since 1.0.0
 */
public interface IChartService {
    List<CmsPost> selectList(CmsPost cmsPost);

    public List<CmsPost> selectListBetweenCreateTime(CmsPost cmsPost,String createTimeBegin,String createTimeEnd);

    public List<CmsComment> selectCmsCommentListBetweenCreateTime(CmsComment cmsComment,String createTimeBegin,String createTimeEnd);


    public List<CmsMessage> selectCmsMessageListBetweenCreateTime(CmsMessage cmsMessage,String createTimeBegin,String createTimeEnd);

}

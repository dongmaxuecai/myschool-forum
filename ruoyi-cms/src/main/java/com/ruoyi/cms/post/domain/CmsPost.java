package com.ruoyi.cms.post.domain;

import com.ruoyi.cms.tag.domain.CmsTag;
import com.ruoyi.cms.type.domain.CmsType;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * 帖子管理对象 cms_post
 * @author toumakazusa
 * @since 1.0.0
 */
public class CmsPost extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;

    /**
     * 标题
     */
    @Excel(name = "标题")
    private String title;

    /**
     * 类型 1帖子 2随笔
     */
    @Excel(name = "类型")
    private String type;

    /**
     * 内容
     */
    @Excel(name = "内容")
    private String content;

    /**
     * 置顶（0否 1是）
     */
    @Excel(name = "置顶", readConverterExp = "0=否,1=是")
    private String top;

    /**
     * 阅读
     */
    @Excel(name = "阅读")
    private Long views;

    /**
     * 状态（0暂存 1发布）
     */
    @Excel(name = "状态", readConverterExp = "0=暂存,1=发布")
    private String status;

    /** 首页图片 */
    @Excel(name = "首页图片")
    private String postPic;

    /** 简介 */
    @Excel(name = "简介")
    private String postDesc;

    /** 附件列表 */
    private String postFiles;

    /**
     * 分类
     */
    private Long[] typeIds;

    /**
     * 分类
     */
    private Long[] tagIds;

    /** 角色对象 */
    private List<CmsTag> tags;

    /** 角色对象 */
    private List<CmsType> types;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTop() {
        return top;
    }

    public void setTop(String top) {
        this.top = top;
    }

    public Long getViews() {
        return views;
    }

    public void setViews(Long views) {
        this.views = views;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPostPic() {
        return postPic;
    }

    public void setPostPic(String postPic) {
        this.postPic = postPic;
    }

    public String getPostDesc() {
        return postDesc;
    }

    public void setPostDesc(String postDesc) {
        this.postDesc = postDesc;
    }

    public String getPostFiles() {
        return postFiles;
    }

    public void setPostFiles(String postFiles) {
        this.postFiles = postFiles;
    }

    public Long[] getTypeIds() {
        return typeIds;
    }

    public void setTypeIds(Long[] typeIds) {
        this.typeIds = typeIds;
    }

    public Long[] getTagIds() {
        return tagIds;
    }

    public void setTagIds(Long[] tagIds) {
        this.tagIds = tagIds;
    }

    public List<CmsTag> getTags() {
        return tags;
    }

    public void setTags(List<CmsTag> tags) {
        this.tags = tags;
    }

    public List<CmsType> getTypes() {
        return types;
    }

    public void setTypes(List<CmsType> types) {
        this.types = types;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", id)
                .append("title", title)
                .append("type", type)
                .append("content", content)
                .append("top", top)
                .append("views", views)
                .append("status", status)
                .append("postPic", postPic)
                .append("postDesc", postDesc)
                .append("postFiles", postFiles)
                .append("typeIds", typeIds)
                .append("tagIds", tagIds)
                .append("tags", tags)
                .append("types", types)
                .toString();
    }
}

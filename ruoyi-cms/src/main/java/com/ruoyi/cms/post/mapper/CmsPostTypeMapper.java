package com.ruoyi.cms.post.mapper;

import com.ruoyi.cms.post.domain.CmsPostTag;
import com.ruoyi.cms.post.domain.CmsPostType;

import java.util.List;

/**
 * post分类关联 数据层
 * @author toumakazusa
 * @since 1.0.0
 */
public interface CmsPostTypeMapper {

    /**
     * 查询分类关联
     * @param cmsPostType
     * @return
     */
    List<CmsPostType> selectPostTypeList(CmsPostType cmsPostType);

    /**
     * 批量插入
     * @param postTypeList
     */
    void batchPostType(List<CmsPostType> postTypeList);

    /**
     * 通过typeId统计post数量
     * @param typeId
     * @return post数量
     */
    int countPostByTypeId(Long typeId);


    /**
     * 通过postID删除post文件关联
     */
    int deletePostTypeByPostId(Long postId);

    /**
     * 通过typeId删除post文件关联
     */
    int deletePostTypeByTypeId(Long typeId);
}

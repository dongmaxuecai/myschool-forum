package com.ruoyi.cms.post.service;

import com.ruoyi.cms.post.domain.CmsPost;
import com.ruoyi.common.core.domain.AjaxResult;

import java.util.List;

/**
 * 帖子管理Service接口
 * @author toumakazusa
 * @since 1.0.0
 */
public interface ICmsPostService {

    /**
     * 查询帖子管理列表
     *
     * @param cmsPost 帖子管理
     * @return 帖子管理集合
     */
    List<CmsPost> selectCmsPostList(CmsPost cmsPost);


    /**
     * 新增帖子管理
     *
     * @param cmsPost 帖子管理
     * @return 结果
     */
    Long insertCmsPost(CmsPost cmsPost);

    /**
     * 根据id查询帖子
     *
     * @param id 帖子管理主键
     * @return 帖子管理
     */
    CmsPost selectCmsPostById(Long id);

    /**
     * 批量删除帖子管理
     *
     * @param ids 需要删除的帖子管理主键集合
     * @return 结果
     */
    int deleteCmsPostByIds(Long[] ids);

    /**
     * 修改帖子管理
     *
     * @param cmsPost 帖子管理
     * @return 结果
     */
    int updateCmsPost(CmsPost cmsPost);

    /**
     * 查询推荐帖子列表
     */
    public List<CmsPost> selectCmsPostListRecommend(CmsPost cmsPost);

    /**
     * 按分类查询帖子列表
     */
    public List<CmsPost> selectCmsPostListByTypeId(Long id);

    /**
     * 按标签查询帖子列表
     */
    public List<CmsPost> selectCmsPostListByTagId(Long id);

    /**
     * 获取帖子详情信息
     * @return
     */
    AjaxResult getInfoDetail(Long id);

    int updatePostPageview(String postId, Long size);
}

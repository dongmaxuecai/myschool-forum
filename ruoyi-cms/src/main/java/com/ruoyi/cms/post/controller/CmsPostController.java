package com.ruoyi.cms.post.controller;

import com.ruoyi.cms.fileinfo.service.ISysFileInfoService;
import com.ruoyi.cms.post.domain.CmsPost;
import com.ruoyi.cms.post.service.ICmsPostService;
import com.ruoyi.cms.tag.domain.CmsTag;
import com.ruoyi.cms.tag.service.ICmsTagService;
import com.ruoyi.cms.type.domain.CmsType;
import com.ruoyi.cms.type.service.ICmsTypeService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.service.SysPermissionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author toumakazusa
 * @since 1.0.0
 */
@RestController
@RequestMapping("/cms/post")
@Api(tags = "帖子管理")
public class CmsPostController extends BaseController {

    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private ICmsPostService cmsPostService;

    @Autowired
    private ICmsTypeService cmsTypeService;

    @Autowired
    private ICmsTagService cmsTagService;

    @Autowired
    private ISysFileInfoService sysFileInfoService;

    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * 首页按标签查询帖子列表
     */
    @GetMapping("/cms/cmsListByTag/{id}")
    public TableDataInfo cmsListByTagId(@PathVariable(value = "id", required = false) Long id) {
        startPage();
        List<CmsPost> list = cmsPostService.selectCmsPostListByTagId(id);
        return getDataTable(list);
    }

    /**
     * 首页按分类查询帖子列表
     */
    @GetMapping("/cms/cmsListByType/{id}")
    public TableDataInfo cmsListByTypeId(@PathVariable(value = "id", required = false) Long id) {
        startPage();
        List<CmsPost> list = cmsPostService.selectCmsPostListByTypeId(id);
        return getDataTable(list);
    }

    /**
     * 首页查询帖子列表
     */
    @GetMapping(value = {"/cms/cmsList", "/cms/cmsEssayList"})
    public TableDataInfo cmsList(CmsPost cmsPost) {
        System.out.println("pageNum:"+ ServletUtils.getParameter("pageNum"));
        startPage();
        //状态为发布
        cmsPost.setStatus("1");
        List<CmsPost> list = cmsPostService.selectCmsPostList(cmsPost);
        return getDataTable(list);
    }

    /**
     * 首页获取帖子详细信息
     */
    @GetMapping(value = {"/cms/detail/", "/cms/detail/{id}"})
    public AjaxResult getInfoDetail(@PathVariable(value = "id", required = false) Long id) {
        return cmsPostService.getInfoDetail(id);
    }

    /**
     * 首页查询推荐帖子列表
     */
    @GetMapping("/cms/cmsListRecommend")
    public TableDataInfo cmsListRecommend(CmsPost cmsPost) {
        startPage();
        //状态为发布
        cmsPost.setStatus("1");
        List<CmsPost> list = cmsPostService.selectCmsPostListRecommend(cmsPost);
        return getDataTable(list);
    }

//    /**
//     * 随笔页查询帖子列表
//     */
//    @GetMapping("/cms/cmsEssayList")
//    public TableDataInfo cmsEssayList(CmsPost cmsPost)
//    {
//        startPage();
//        //状态为发布
//        cmsPost.setStatus("1");
//        List<CmsPost> list = cmsPostService.selectCmsPostList(cmsPost);
//        return getDataTable(list);
//    }

    /**
     * 查询帖子管理列表
     */
    @GetMapping("/list")
    @PreAuthorize("@ss.hasPermi('cms:post:list')")
    @ApiOperation("分页查询")
    public TableDataInfo list(CmsPost cmsPost) {
        Set<String> roles = permissionService.getRolePermission(getLoginUser().getUser());
        if (!SecurityUtils.isAdmin(getUserId()) && !roles.contains("admin") && !roles.contains("cms")) {
            cmsPost.setCreateBy(getUsername());
        }
        startPage();
        List<CmsPost> list = cmsPostService.selectCmsPostList(cmsPost);
        return getDataTable(list);
    }

    /**
     * 新增帖子管理
     */
    @PreAuthorize("@ss.hasPermi('cms:post:add')")
    @Log(title = "帖子管理", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation("新增帖子")
    public AjaxResult add(@RequestBody CmsPost cmsPost) {
        cmsPost.setCreateBy(getUsername());
        Long postId = cmsPostService.insertCmsPost(cmsPost);
        if (postId == null) {
            return AjaxResult.error();
        }
        return AjaxResult.success(postId);
    }

    /**
     * 获取标签和类型
     * 如果传入portId就查询用于回显
     */
    @PreAuthorize("@ss.hasPermi('cms:post:query')")
    @GetMapping(value = {"/", "/{id}"})
    @ApiOperation("获取标签和类型")
    public AjaxResult getInfo(@PathVariable(value = "id", required = false) Long id) {
        AjaxResult ajax = AjaxResult.success();
        CmsType cmsType = new CmsType();
        CmsTag cmsTag = new CmsTag();
        ajax.put("types", cmsTypeService.selectCmsTypeList(cmsType));
        ajax.put("tags", cmsTagService.selectCmsTagList(cmsTag));
        if (StringUtils.isNotNull(id)) {
            ajax.put(AjaxResult.DATA_TAG, cmsPostService.selectCmsPostById(id));
        }
        return ajax;
    }


    /**
     * 删除帖子管理
     */
    @PreAuthorize("@ss.hasPermi('cms:post:remove')")
    @Log(title = "帖子管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    @ApiOperation("批量删除帖子")
    public AjaxResult remove(@PathVariable Long[] ids) {
        //删除原首图
        for (Long id : ids) {
            CmsPost oldPost = cmsPostService.selectCmsPostById(id);
            if (oldPost != null && !StringUtils.isBlank(oldPost.getPostPic())) {
                String postPic = oldPost.getPostPic();
                int newFileNameSeparatorIndex = postPic.lastIndexOf("/");
                String fileName = postPic.substring(newFileNameSeparatorIndex + 1).toLowerCase();
                sysFileInfoService.deleteSysFileInfoByFileObjectName(fileName);
            }
        }
        return toAjax(cmsPostService.deleteCmsPostByIds(ids));
    }

    /**
     * 修改帖子管理
     */
    @PreAuthorize("@ss.hasPermi('cms:post:edit')")
    @Log(title = "帖子管理", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation("修改帖子")
    public AjaxResult edit(@RequestBody CmsPost cmsPost) {
        cmsPost.setUpdateBy(getUsername());
        //删除原首图
        CmsPost oldPost = cmsPostService.selectCmsPostById(cmsPost.getId());
        if (cmsPost.getPostPic().isEmpty() || !cmsPost.getPostPic().equals(oldPost.getPostPic())) {
            if (!oldPost.getPostPic().isEmpty()) {
                String postPic = oldPost.getPostPic();
                if (postPic != null && !"".equals(postPic)) {
                    int newFileNameSeparatorIndex = postPic.lastIndexOf("/");
                    String FileName = postPic.substring(newFileNameSeparatorIndex + 1).toLowerCase();
                    sysFileInfoService.deleteSysFileInfoByFileObjectName(FileName);
                }
            }
        }
        return toAjax(cmsPostService.updateCmsPost(cmsPost));
    }


    /**
     * 导出帖子管理列表
     */
    @PreAuthorize("@ss.hasPermi('cms:post:export')")
    @Log(title = "帖子管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation("导出帖子")
    public void export(HttpServletResponse response, CmsPost cmsPost) {
        List<CmsPost> list = cmsPostService.selectCmsPostList(cmsPost);
        ExcelUtil<CmsPost> util = new ExcelUtil<CmsPost>(CmsPost.class);
        util.exportExcel(response, list, "帖子管理数据");
    }

    /**
     * 取消按钮-删除首图
     * 如果postPic不为空,
     * - 没有id, 说明新增,可以直接删除该图片
     * - 有id, 说明修改,对比原来的图片, 如果不一致则删除
     */
    @PreAuthorize("@ss.hasPermi('cms:post:edit')")
    @PostMapping("/cancel")
    @ApiOperation("取消按钮-删除首图")
    public AjaxResult cancel(@RequestBody CmsPost cmsPost) {
        String postPic = cmsPost.getPostPic();
        if (postPic != null && !"".equals(postPic)) {
            Long postId = cmsPost.getId();
            if (postId == null) {
                int newFileNameSeparatorIndex = postPic.lastIndexOf("/");
                String FileName = postPic.substring(newFileNameSeparatorIndex + 1).toLowerCase();
                sysFileInfoService.deleteSysFileInfoByFileObjectName(FileName);
            } else {
                String Pic = cmsPostService.selectCmsPostById(postId).getPostPic();
                if (!postPic.equals(Pic)) {
                    int newFileNameSeparatorIndex = postPic.lastIndexOf("/");
                    String FileName = postPic.substring(newFileNameSeparatorIndex + 1).toLowerCase();
                    sysFileInfoService.deleteSysFileInfoByFileObjectName(FileName);
                }
            }
        }
        return toAjax(1);
    }

}

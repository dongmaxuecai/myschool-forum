package com.ruoyi.cms.post.mapper;

import com.ruoyi.cms.post.domain.CmsPostTag;

import java.util.List;

/**
 * 〈post标签关联 数据层〉
 * @author toumakazusa
 * @since 1.0.0
 */
public interface CmsPostTagMapper {

    /**
     * 查询标签关联
     */
    public List<CmsPostTag> selectPostTagList(CmsPostTag cmsPostTag);

    /**
     * 批量插入
     * @param postTagList
     */
    void batchPostTag(List<CmsPostTag> postTagList);

    /**
     * 通过tagId统计post数量
     */
    int countPostByTagId(Long tagId);

    /**
     * 通过postID删除post文件关联
     */
    int deletePostTagByPostId(Long postId);

    /**
     * 通过tagId删除post文件关联
     */
    int deletePostTagByTagId(Long tagId);
}

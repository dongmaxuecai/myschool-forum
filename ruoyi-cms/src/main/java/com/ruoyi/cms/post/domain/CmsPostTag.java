package com.ruoyi.cms.post.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 〈post标签关联表 cms_post_tag〉
 * @author toumakazusa
 * @since 1.0.0
 */
public class CmsPostTag {
    private Long tagId;
    private Long postId;
    private Long[] tagIds;

    public CmsPostTag() {
    }

    public CmsPostTag(Long tagId, Long postId) {
        this.tagId = tagId;
        this.postId = postId;
    }

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public Long[] getTagIds() {
        return tagIds;
    }

    public void setTagIds(Long[] tagIds) {
        this.tagIds = tagIds;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("tagId", getTagId())
                .append("postId", getPostId())
                .toString();
    }
}
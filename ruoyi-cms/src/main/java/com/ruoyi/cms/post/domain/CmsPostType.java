package com.ruoyi.cms.post.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 〈post类型关联表 cms_post_type〉
 * @author toumakazusa
 * @since 1.0.0
 */
public class CmsPostType {
    private Long typeId;
    private Long postId;
    private Long[] typeIds;

    public CmsPostType() {
    }

    public CmsPostType(Long typeId, Long postId) {
        this.typeId = typeId;
        this.postId = postId;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public Long[] getTypeIds() {
        return typeIds;
    }

    public void setTypeIds(Long[] typeIds) {
        this.typeIds = typeIds;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("typeId", typeId)
                .append("postId", postId)
                .toString();
    }
}

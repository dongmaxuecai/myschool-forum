package com.ruoyi.cms.post.mapper;

import com.ruoyi.cms.post.domain.CmsPost;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 帖子管理Mapper接口
 * @author toumakazusa
 * @since 1.0.0
 */
public interface CmsPostMapper {


    /**
     * 条件查询帖子列表
     * @param cmsPost
     * @return
     */
    public List<CmsPost> selectCmsPostList(CmsPost cmsPost);

    /**
     * 新增帖子
     *
     * @param cmsPost 帖子管理
     * @return 结果
     */
    void insertCmsPost(CmsPost cmsPost);

    /**
     * 根据id查询帖子
     * @param id
     * @return
     */
    CmsPost selectCmsPostById(Long id);

    /**
     * 批量删除帖子管理
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    int deleteCmsPostByIds(Long[] ids);

    /**
     * 修改帖子管理
     *
     * @param cmsPost 帖子管理
     * @return 结果
     */
    int updateCmsPost(CmsPost cmsPost);

    public List<CmsPost> selectCmsPostListBetweenCreateTime(@Param("title") String title, @Param("type") String type, @Param("top") String top, @Param("status") String status, @Param("createTimeBegin") String createTimeBegin, @Param("createTimeEnd") String createTimeEnd, @Param("createBy") String createBy);

    /**
     * 查询推荐帖子列表
     */
    public List<CmsPost> selectCmsPostListRecommend(CmsPost cmsPost);

    /**
     * 按分类查询帖子管理列表
     */
    public List<CmsPost> selectCmsPostListByTypeId(Long id);

    /**
     * 按标签查询帖子管理列表
     */
    List<CmsPost> selectCmsPostListByTagId(Long id);

    /**
     * 更新帖子阅读量
     * @param postId
     * @param increment
     * @return
     */
    int updatePostPageview(@Param("postId") String postId,@Param("increment") Long increment);
}

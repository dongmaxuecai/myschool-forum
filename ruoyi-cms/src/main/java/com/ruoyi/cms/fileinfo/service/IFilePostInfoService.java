package com.ruoyi.cms.fileinfo.service;

import com.ruoyi.cms.fileinfo.domain.FilePostInfo;

import java.util.List;

/**
 * @author toumakazusa
 * @since 1.0.0
 */
public interface IFilePostInfoService {


    /**
     * 查询文件列表
     */
    List<FilePostInfo> selectFilePostList(Long postId);

    /**
     * 批量删除通知公告和文件关联
     */
    int deleteFilePost(Long[] postIds);
}

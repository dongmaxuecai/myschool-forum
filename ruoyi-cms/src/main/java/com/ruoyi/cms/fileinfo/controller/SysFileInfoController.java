package com.ruoyi.cms.fileinfo.controller;

import com.ruoyi.cms.fileinfo.domain.SysFileInfo;
import com.ruoyi.cms.fileinfo.service.ISysFileInfoService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.web.service.SysPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * 文件管理Controller
 * @author toumakazusa
 * @since 1.0.0
 */
@RestController
@RequestMapping("/fileInfo")
public class SysFileInfoController extends BaseController {

    @Autowired
    private ISysFileInfoService sysFileInfoService;

    @Autowired
    private SysPermissionService permissionService;
    /**
     * 查询文件管理列表
     */
    @PreAuthorize("@ss.hasPermi('cms:fileInfo:list')")

    @GetMapping("/list")
    public TableDataInfo list(SysFileInfo sysFileInfo) {
        startPage();
        //判断用户权限
        String createBy = sysFileInfo.getCreateBy();
        if (createBy == null || "".equals(createBy)) {
            throw new ServiceException("获取列表createBy参数为空");
        }
        // 角色集合
        Set<String> roles = permissionService.getRolePermission(getLoginUser().getUser());
        if (SecurityUtils.isAdmin(getUserId()) || roles.contains("admin") || roles.contains("cms")) {
            sysFileInfo.setCreateBy("");
        }
        List<SysFileInfo> list = sysFileInfoService.selectSysFileInfoList(sysFileInfo);
        return getDataTable(list);
    }

    /**
     * 删除文件管理
     */
    @PreAuthorize("@ss.hasPermi('cms:fileInfo:remove')")
    @Log(title = "文件管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{fileIds}")
    public AjaxResult remove(@PathVariable Long[] fileIds) {
        return toAjax(sysFileInfoService.deleteSysFileInfoByFileIds(fileIds));
    }

}

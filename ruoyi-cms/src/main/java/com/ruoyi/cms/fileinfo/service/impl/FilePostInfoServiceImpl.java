package com.ruoyi.cms.fileinfo.service.impl;

import com.ruoyi.cms.fileinfo.domain.FilePostInfo;
import com.ruoyi.cms.fileinfo.mapper.FilePostInfoMapper;
import com.ruoyi.cms.fileinfo.service.IFilePostInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author toumakazusa
 * @since 1.0.0
 */
@Service
public class FilePostInfoServiceImpl implements IFilePostInfoService {

    @Autowired
    private FilePostInfoMapper filePostInfoMapper;

    @Override
    public List<FilePostInfo> selectFilePostList(Long postId) {
        return filePostInfoMapper.selectFilePostList(postId);
    }

    @Override
    public int deleteFilePost(Long[] ids) {
        return filePostInfoMapper.deleteFilePost(ids);
    }
}

package com.ruoyi.cms.fileinfo.mapper;

import com.ruoyi.cms.fileinfo.domain.FilePostInfo;

import java.util.List;

/**
 * 〈post文件关联 数据层〉
 * @author toumakazusa
 * @since 1.0.0
 */
public interface FilePostInfoMapper {
    /**
     * 查询文件列表
     */
    List<FilePostInfo> selectFilePostList(Long postId);

    /**
     * 批量删除post文件关联
     */
    int deleteFilePost(Long[] ids);
}

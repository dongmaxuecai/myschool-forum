package com.ruoyi.cms.fileinfo.service;

import com.ruoyi.cms.fileinfo.domain.SysFileInfo;

import java.util.List;

/**
 * 文件管理Service接口
 * @author toumakazusa
 * @since 1.0.0
 */
public interface ISysFileInfoService {

    /**
     * 删除文件管理信息
     *
     * @param fileObjectName
     * @return 结果
     */
    int deleteSysFileInfoByFileObjectName(String fileObjectName);

    /**
     * 删除文件管理信息
     *
     * @param fileId 文件管理主键
     * @return 结果
     */
    int deleteSysFileInfoByFileId(Long fileId);

    /**
     * 查询文件管理
     *
     * @param fileId 文件管理主键
     * @return 文件管理
     */
    SysFileInfo selectSysFileInfoByFileId(Long fileId);

    /**
     * 查询文件管理列表
     *
     * @param sysFileInfo 文件管理
     * @return 文件管理集合
     */
    public List<SysFileInfo> selectSysFileInfoList(SysFileInfo sysFileInfo);

    /**
     * 新增文件管理
     *
     * @param sysFileInfo 文件管理
     * @return 结果
     */
    public int insertSysFileInfo(SysFileInfo sysFileInfo);

    /**
     * 批量删除文件管理
     *
     * @param fileIds 需要删除的文件管理主键集合
     * @return 结果
     */
    public int deleteSysFileInfoByFileIds(Long[] fileIds);
}

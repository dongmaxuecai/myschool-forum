package com.ruoyi.cms.comment.mapper;

import com.ruoyi.cms.comment.domain.CmsCommentLike;

import java.util.List;

/**
 * @author toumakazusa
 * @since 1.0.0
 */
public interface CmsCommentLikeMapper {

    /**
     * 查询列表
     */
    public List<CmsCommentLike> selectCmsCommentLikeList(CmsCommentLike cmsCommentLike);
}

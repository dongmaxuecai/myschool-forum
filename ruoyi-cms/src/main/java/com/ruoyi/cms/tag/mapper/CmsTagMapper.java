package com.ruoyi.cms.tag.mapper;

import com.ruoyi.cms.tag.domain.CmsTag;

import java.util.List;

/**
 * @author toumakazusa
 * @since 1.0.0
 */
public interface CmsTagMapper {
    /**
     * 根据id列表查询CmsTag
     * @param tagIds
     * @return
     */
    List<CmsTag> selectCmsTagByIds(List<Long> tagIds);

    /**
     * 查询标签管理列表
     *
     * @param cmsTag 标签管理
     * @return 标签管理集合
     */
    List<CmsTag> selectCmsTagList(CmsTag cmsTag);

    /**
     * 查询标签管理
     *
     * @param tagId 标签管理主键
     * @return 标签管理
     */
    CmsTag selectCmsTagByTagId(Long tagId);

    /**
     * 通过tagName查询标签管理列表
     *
     * @param tagName
     * @return 标签管理集合
     */
    List<CmsTag> selectCmsTagListByTagName(String tagName);

    /**
     * 新增标签管理
     *
     * @param cmsTag 标签管理
     * @return 结果
     */
    int insertCmsTag(CmsTag cmsTag);

    /**
     * 修改标签管理
     *
     * @param cmsTag 标签管理
     * @return 结果
     */
    int updateCmsTag(CmsTag cmsTag);

    /**
     * 批量删除标签管理
     *
     * @param tagIds 需要删除的数据主键集合
     * @return 结果
     */
    int deleteCmsTagByTagIds(Long[] tagIds);
}

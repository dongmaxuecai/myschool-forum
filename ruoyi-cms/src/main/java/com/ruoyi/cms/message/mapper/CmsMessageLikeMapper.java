package com.ruoyi.cms.message.mapper;

import com.ruoyi.cms.message.domain.CmsMessageLike;

import java.util.List;

/**
 * 留言点赞mapper层
 * @author toumakazusa
 * @since 1.0.0
 */
public interface CmsMessageLikeMapper {

    /**
     * 查询列表
     */
    List<CmsMessageLike> selectCmsMessageLikeList(CmsMessageLike cmsMessageLike);
}

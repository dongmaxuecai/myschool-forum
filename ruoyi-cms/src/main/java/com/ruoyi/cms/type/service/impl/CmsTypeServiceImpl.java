package com.ruoyi.cms.type.service.impl;

import com.ruoyi.cms.fileinfo.service.ISysFileInfoService;
import com.ruoyi.cms.post.mapper.CmsPostTypeMapper;
import com.ruoyi.cms.type.domain.CmsType;
import com.ruoyi.cms.type.mapper.CmsTypeMapper;
import com.ruoyi.cms.type.service.ICmsTypeService;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author toumakazusa
 * @since 1.0.0
 */
@Service
public class CmsTypeServiceImpl implements ICmsTypeService {

    @Autowired
    private CmsTypeMapper cmsTypeMapper;

    @Autowired
    private CmsPostTypeMapper cmsPostTypeMapper;

    @Autowired
    private ISysFileInfoService sysFileInfoService;

    @Override
    public List<CmsType> selectCmsTypeList(CmsType cmsType) {
        List<CmsType> cmsTypeList = cmsTypeMapper.selectCmsTypeList(cmsType);
        if (cmsTypeList != null && cmsTypeList.size() > 0) {
            for (CmsType type : cmsTypeList) {
                int postNum = cmsPostTypeMapper.countPostByTypeId(type.getTypeId());
                type.setPostNum(postNum);
            }
        }
        return cmsTypeList;
    }

    @Override
    public CmsType selectCmsTypeByTypeId(Long typeId) {
        return cmsTypeMapper.selectCmsTypeByTypeId(typeId);
    }

    @Override
    public int insertCmsType(CmsType cmsType) {
        List<CmsType> cmsTypeList = cmsTypeMapper.selectCmsTypeListByTypeName(cmsType.getTypeName());
        if (cmsTypeList.size() > 0) {
            throw new ServiceException("分类名称已存在");
        }
        cmsType.setCreateTime(DateUtils.getNowDate());
        return cmsTypeMapper.insertCmsType(cmsType);
    }

    @Override
    public int updateCmsType(CmsType cmsType) {
        //更新后的分类名称已存在
        List<CmsType> cmsTypeList = cmsTypeMapper.selectCmsTypeListByTypeName(cmsType.getTypeName());
        if (cmsTypeList.size() > 0) {
            for (CmsType type : cmsTypeList) {
                if (!type.getTypeId().equals(cmsType.getTypeId())) {
                    throw new ServiceException("分类名称已存在");
                }
            }
        }
        //删除原来的分类图片
        String typePic = cmsTypeMapper.selectCmsTypeByTypeId(cmsType.getTypeId()).getTypePic();
        if (typePic != null && !"".equals(typePic) && !typePic.equals(cmsType.getTypePic())) {
            int newFileNameSeparatorIndex = typePic.lastIndexOf("/");
            String FileName = typePic.substring(newFileNameSeparatorIndex + 1).toLowerCase();
            sysFileInfoService.deleteSysFileInfoByFileObjectName(FileName);
        }
        cmsType.setUpdateTime(DateUtils.getNowDate());
        //更新
        return cmsTypeMapper.updateCmsType(cmsType);
    }

    @Override
    public int deleteCmsTypeByTypeIds(Long[] typeIds) {
        for (Long typeId : typeIds) {
            String typePic = cmsTypeMapper.selectCmsTypeByTypeId(typeId).getTypePic();
            if (typePic != null && !"".equals(typePic)) {
                int newFileNameSeparatorIndex = typePic.lastIndexOf("/");
                String FileName = typePic.substring(newFileNameSeparatorIndex + 1).toLowerCase();
                sysFileInfoService.deleteSysFileInfoByFileObjectName(FileName);
            }
            //删除分类帖子关联表信息
            cmsPostTypeMapper.deletePostTypeByTypeId(typeId);
        }
        //删除分类
        return cmsTypeMapper.deleteCmsTypeByTypeIds(typeIds);
    }

    @Override
    public int cancel(CmsType cmsType) {
        String typePic = cmsType.getTypePic();
        if (typePic != null && !"".equals(typePic)) {
            Long typeId = cmsType.getTypeId();
            if (typeId == null) {
                int newFileNameSeparatorIndex = typePic.lastIndexOf("/");
                String FileName = typePic.substring(newFileNameSeparatorIndex + 1).toLowerCase();
                sysFileInfoService.deleteSysFileInfoByFileObjectName(FileName);
            } else {
                String Pic = cmsTypeMapper.selectCmsTypeByTypeId(typeId).getTypePic();
                if (!typePic.equals(Pic)) {
                    int newFileNameSeparatorIndex = typePic.lastIndexOf("/");
                    String FileName = typePic.substring(newFileNameSeparatorIndex + 1).toLowerCase();
                    sysFileInfoService.deleteSysFileInfoByFileObjectName(FileName);
                }
            }
        }
        return 1;
    }


}

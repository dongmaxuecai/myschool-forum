package com.ruoyi.cms.type.controller;

import com.ruoyi.cms.post.domain.CmsPost;
import com.ruoyi.cms.type.domain.CmsType;
import com.ruoyi.cms.type.service.ICmsTypeService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.service.SysPermissionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Set;


/**
 * 分类管理Controller
 *
 * @author toumakazusa
 * @since 1.0.0
 */
@RestController
@RequestMapping("/cms/type")
@Api("分类管理")
public class CmsTypeController extends BaseController {

    @Autowired
    private ICmsTypeService cmsTypeService;

    @Autowired
    private SysPermissionService permissionService;

    /**
     * 查询分类管理列表
     */
    @PreAuthorize("@ss.hasPermi('cms:type:list')")
    @GetMapping("/list")
    @ApiOperation("分类条件查询")
    public TableDataInfo list(CmsType cmsType) {
        startPage();
        // 角色集合
        Set<String> roles = permissionService.getRolePermission(getLoginUser().getUser());
        if (!SecurityUtils.isAdmin(getUserId()) && !roles.contains("admin") && !roles.contains("cms")) {
            cmsType.setCreateBy(getUsername());
        }
        List<CmsType> list = cmsTypeService.selectCmsTypeList(cmsType);
        return getDataTable(list);
    }

    /**
     * 获取分类管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('cms:type:query')")
    @GetMapping(value = "/{typeId}")
    @ApiOperation("根据id查询分类")
    public AjaxResult getInfo(@PathVariable("typeId") Long typeId)
    {
        return AjaxResult.success(cmsTypeService.selectCmsTypeByTypeId(typeId));
    }


    /**
     * 新增分类管理
     */
    @PreAuthorize("@ss.hasPermi('cms:type:add')")
    @Log(title = "分类管理", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation("分类新增")
    public AjaxResult add(@RequestBody CmsType cmsType)
    {
        cmsType.setCreateBy(getUsername());
        return toAjax(cmsTypeService.insertCmsType(cmsType));
    }

    /**
     * 修改分类管理
     */
    @PreAuthorize("@ss.hasPermi('cms:type:edit')")
    @Log(title = "分类管理", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation("修改分类")
    public AjaxResult edit(@RequestBody CmsType cmsType)
    {
        cmsType.setUpdateBy(getUsername());
        return toAjax(cmsTypeService.updateCmsType(cmsType));
    }

    /**
     * 删除分类管理
     */
    @PreAuthorize("@ss.hasPermi('cms:type:remove')")
    @Log(title = "分类管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{typeIds}")
    @ApiOperation("批量删除分类")
    public AjaxResult remove(@PathVariable Long[] typeIds)
    {
        return toAjax(cmsTypeService.deleteCmsTypeByTypeIds(typeIds));
    }

    /**
     * 取消按钮-删除分类图片
     */
    @PreAuthorize("@ss.hasPermi('cms:type:edit')")
    @PostMapping("/cancel")
    @ApiOperation("取消-删除图片")
    public AjaxResult cancel(@RequestBody CmsType cmsType)
    {
        return toAjax(cmsTypeService.cancel(cmsType));
    }

    /**
     * 导出分类管理列表
     */
    @PreAuthorize("@ss.hasPermi('cms:post:export')")
    @Log(title = "分类管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation("导出分类")
    public void export(HttpServletResponse response, CmsType cmsType) {
        List<CmsType> list = cmsTypeService.selectCmsTypeList(cmsType);
        ExcelUtil<CmsType> util = new ExcelUtil<>(CmsType.class);
        util.exportExcel(response, list, "分类管理数据");
    }


}
